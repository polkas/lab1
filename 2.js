const score = (text) => {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.'";
    let p = 0;
    text.split('').map( item => {
        if (charset.indexOf(item) !== -1 || item === ' ' || item === '\'') {
            p++;
        }
    });
    return p;
}

const xor = (str, key) => {
    c = '';
    for(i=0; i<str.length; i++) {
        c += String.fromCharCode(str[i].charCodeAt(0).toString(10) ^ key.charCodeAt(0).toString(10));
    }
    return c;
}

function hex2a(hexx) {
    var hex = hexx.toString();
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function toHex(str) {
	var hex = '';
	for(var i=0;i<str.length;i++) {
		hex += ''+str.charCodeAt(i).toString(16);
	}
	return hex;
}

const xorWithString = (text, key) => {
    const a = Math.ceil(text.length / key.length);
    let str = '';
    // console.log(text);
    for(let i = 0; i < a; i++) {
        str = str.concat(key);
    }
    let result = '';
    for(let i = 0; i < text.length; i++) {
        result = result.concat(xor(text[i], str[i]));
    }

    console.log(result);
}


const countLetters = (text) => {
    const letters = {};
    for(let i = 0; i < text.length; i++) {
        if (letters[text[i]]) {
            letters[text[i]]++;    
        } else {
            letters[text[i]] = 1;    
        }
    }
    console.log(letters);
};

const indexOfCoincidences = (text) => {
    const shiftedStrings = [];
    let helperString = text;
    for(let i = 0; i < 25; i++) {
        const char = helperString[0];
        helperString = helperString.substring(1) + char;
        shiftedStrings.push(helperString);
    }
    const indexes = [];
    for(let i = 0; i < shiftedStrings.length; i++) {
        let index = 0;
        for(let j = 0; j < text.length; j++) {
            if (text[j] === shiftedStrings[i][j]) {
                index++;
            }
        }
        indexes.push(index);
    }
    console.log(indexes);
}

const devideAndXor = (text, indexOfCoincidences) => {
    const devidedStrings = [];
    for (let i = 0; i < indexOfCoincidences; i++) {
        devidedStrings.push('');
    }
    for(let i = 0; i < text.length; i++) {
        const index = i % indexOfCoincidences;
        devidedStrings[index] = devidedStrings[index].concat(text[i]);
    }
    let bests = '';
    devidedStrings.map( str => {
        let best = "";
        b = 0;
        char = '';
        for(let j = 1; j <= 256; j++) {
            const c = xor(str, String.fromCharCode(j));
            const scr = score(c);
            if (scr > b) {
                b = scr;
                best = c;
                char = String.fromCharCode(j);
            }
        }
        bests = bests.concat(char);
    });
    console.log(bests);

    xorWithString(text, bests);
}

indexOfCoincidences(hex2a("1b420538554c2d100f2354096c44036c511838510f27101f235d096c43052140f554c3858096c530321400029480538494c23564c3858053f100322554c3b55f1c4c3f5f4c3858096c5b0935431c2d53096c591f6c5f0220494c7e064d6c64036c570938101824591f6c5f0229101e25570438100d39440321511825530d205c156c490339101b255c006c401e23520d2e5c156c5e0929544c385f4c3943096c430321554c3f5f1e3810032a100b295e0938590f6c51002b5f1e254404211c4c3f5901395c0d3855086c510222550d2059022b10033e100b3e510825550238100829430f295e1862103f29420523451f2049406c471e2544096c59186c42052b58186c5e033b1c4c355f196c4705205c4c2255092810053810182310082953053c58093e101824554c22551438100322554c2d434c3b5500201e4c0e550d3e1005221001255e0860101824551e29171f6c5e036c431c2d53093f1e"));
devideAndXor(hex2a("1b420538554c2d100f2354096c44036c511838510f27101f235d096c430521400029101f39521f385918394405235e4c2f591c24551e62103823101e2954192f554c3858096c530321400029480538494c23564c3858053f100322554c3b554c3b59002010193f554c235e003510193c40093e530d3f554c20551838551e3f1c4c3f5f4c3858096c5b0935431c2d53096c591f6c5f0220494c7e064d6c64036c570938101824591f6c5f0229101e25570438100d39440321511825530d205c156c490339101b255c006c401e23520d2e5c156c5e0929544c385f4c3943096c430321554c3f5f1e3810032a100b295e0938590f6c51002b5f1e254404211c4c3f5901395c0d3855086c510222550d2059022b10033e100b3e510825550238100829430f295e1862103f29420523451f2049406c471e2544096c59186c42052b58186c5e033b1c4c355f196c4705205c4c2255092810053810182310082953053c58093e101824554c22551438100322554c2d434c3b5500201e4c0e550d3e1005221001255e0860101824551e29171f6c5e036c431c2d53093f1e"), 3);